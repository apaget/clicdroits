import os
import importlib
import string;
from Database import Database
import json
import time
import random;
import datetime
import requests

# 1 SituationGenerator par admin

db = Database()

def get_json(url):
    requete = requests.get(url.encode(encoding="UTF-8"))
    if requete.status_code != 200:
        error_list.append(url + " from : " + "https://legislation.openfisca.fr/" + parent);
        return {"error" : "server Connection "}
    page = requete.content
    json_data = json.loads(page, encoding=requete.encoding);
    return json_data

js = get_json('https://fr.openfisca.org/api/v21/variables')
for k in js:
    data =  get_json('https://fr.openfisca.org/api/v21/variable/' + k)
    if 'possibleValues' in  data:
        db.update('variables_template', {'enum' : json.dumps(data['possibleValues'])}, where=["name='"+data['id'] + "'"]);
        db.commit()
        print k, data['possibleValues'] 
