import os
import importlib
import string;
from Database import Database
import json
import time
import random;
import datetime

# 1 SituationGenerator par admin

db = Database()

def rand_string(len):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(0, 10))

class SituationGenerator(object):

    """Docstring for SituationGenerator. """

    req = {'familles' :  "select id, ref, conjoint, enfants from menage where ref = {0} or conjoint = {0} or enfants REGEXP '([^0-9]|^){0}([^0-9]|$)';"};
    def __init__(self, id, aide):
        self.aide = aide;

        (id_m, red, conj, str_enfant) = db.query(self.req['familles'].format(id)).fetchone();
        id_m = str(id_m)
        self.id = id;
        self.indiv = [{'id': id, 'famille' : id_m, 'foyer_fiscal' : id_m , 'menage' : id_m}]
        self.modules = {}


        self.json = self.load_basic_situation();
        self.load_situation()
        for _id in self.indiv:
            self.build_individu(_id['id'])
        """TODO: to be defined1. """
    
    def load_basic_situation(self):
        return {"individus" : {}, "familles" : {}, "foyers_fiscaux" : {}, "menages" : {}};

    def add_id(self, newid, famille, foyer, menage):
        if newid == '':
            return
        newid = int(newid)
        for id in self.indiv:
            if newid == id['id']:
                return
        self.indiv.append({'id': str(newid), 'famille' : str(famille), 'foyer_fiscal' : str(foyer) , 'menage' : str(menage)});
        

    ## Construction de la situation pour un id donner
    def load_situation(self):
        data  =  db.query(self.req['familles'].format(self.id)).fetchone();
        self.json['familles'] = self.build_familles(data)
        self.json['foyers_fiscaux'] = self.build_foyer_fiscal(data)
        self.json["menages"] = self.build_menage(data)

       ## 1 etape construire les 3 groupes
    def build_familles(self, data):
        (id_m, red, conj, str_enfant) = data;
        self.add_id(red, id_m, id_m, id_m)
        self.add_id(conj, id_m, id_m, id_m)
        for child in str_enfant.split(':'):
            self.add_id(child, id_m, id_m, id_m)
        ret = {str(id_m) : {
            'parents' : [str(red), str(conj)]
            }};
        if len(str_enfant.split(':')) > 0 and str_enfant.split(':')[0] != '':
            ret[str(id_m)]['enfants'] = [str(child) for child in str_enfant.split(':')]
        return ret;

    def build_foyer_fiscal(self, data):
        (_, red, conj, str_enfant) = data;
        ret = {str(_) : {
            'declarants' : [str(red), str(conj)]
            }};
        if len(str_enfant.split(':')) > 0 and str_enfant.split(':')[0] != '':
            ret[str(_)]['personnes_a_charge'] = [str(child) for child in str_enfant.split(':')]
        return ret;


    def build_menage(self, data):
        (_, red, conj, str_enfant) = data;
        ret = {str(_) : {
            'personne_de_reference' : [str(red)]
            }};
        if conj:
            ret[str(_)]['conjoint'] = [str(conj)]
        if len(str_enfant.split(':')) > 0 and str_enfant.split(':')[0] != '':
            ret[str(_)]['enfants'] = [str(child) for child in str_enfant.split(':')]
        return ret;


    def get_indiv(self, id):
        for a in self.indiv:
            if a['id'] == id:
                return a
        return None
    
    ## 2eme etape build chaque individu
    def build_individu(self, id):
        prev_month = datetime.date.today() + datetime.timedelta(-random.randint(18, 60)*365)
        self.json['individus'][str(id)] = {'date_naissance' : {'ETERNITY' : prev_month.strftime("%Y-%m-%d")}}
        for file in os.listdir("../client/" + self.aide):
            if file.endswith(".py") and file != "__init__.py":
                self.build_module(importlib.import_module("rsa." + file.split('.')[0]), self.get_indiv(id))

    def build_module(self, module, indiv):
        if not module or not indiv:
            return
        class_module = getattr(module, module.desc["name"] + "_loader")();
        data = {}
        for k, v in class_module.sql_req.items():
            data[k] = db.query(v.format(indiv['id']))
        if (module.desc["entity"] == 'famille'):
            self.json['familles'][indiv['famille']][module.desc["name"]] = class_module.load(data);
        elif (module.desc["entity"] == 'foyer_fiscal'):
            self.json['familles'][indiv['foyer_fiscal']][module.desc["name"]] = class_module.load(data);
        elif (module.desc["entity"] == 'menage'):
            self.json['familles'][indiv['menage']][module.desc["name"]] = class_module.load(data);
        elif (module.desc["entity"] == 'individu'):
            self.json['individus'][str(indiv['id'])][module.desc["name"]] = class_module.load(data);
    
    def get_json(self):
        return self.json





