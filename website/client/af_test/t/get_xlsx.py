from openpyxl import Workbook
import json
from openpyxl.styles import colors
from openpyxl.styles import Font, Color

wb = Workbook()
ws = wb.active

def itoa(n):
    string = ""
    while n > 0:
        n, remainder = divmod(n - 1, 26)
        string = chr(65 + remainder) + string
    return string


with open('test_clement') as target:
    js = json.loads(target.read())


m = 3

for i in range(0, 2000):
    if js[i]['status'] == 1 or js[i]['status'] == 2:
        ws[itoa(1) + str(i + m)].font = Font(color=colors.GREEN)
    elif js[i]['status'] == 3:
        ws[itoa(1) + str(i + m)].font = Font(color='FFFFA500')
    else:
        ws[itoa(1) + str(i + m)].font = Font(color=colors.RED)
    ws[itoa(1) + str(i + m)] = js[i]['data'][0]
    ws[itoa(2) + str(i + m)] = js[i]['data'][2]
    ws[itoa(3) + str(i + m)] = js[i]['val']
    for y in range(1, len(js[i]['data'])):
        #print(itoa(y) + str(i))
        ws[itoa(y + 3) + str(i + m)] = js[i]['data'][y]

ok = 0
nonr = 0
fail = 0
for i in range(2000):
    if js[i]['status'] == 1 or js[i]['status'] == 2:
        ok += 1
    elif js[i]['status'] == 4:
        nonr += 1
    else:
        fail += 1
        pass
ws['A1'] = str(ok) + ' ok'
ws['A1'].font = Font(color=colors.GREEN)
ws['B1'] = str(nonr) + ' non-recourants'
ws['B1'].font = Font(color=colors.RED)
ws['C1'] = str(fail) + ' erreurs'
ws['C1'].font = Font(color='FFFFA500')

wb.save('test.xlsx')
