import sys
import json

def intab(tab, v):
    for t in tab:
        if t['name'] == v:
            return 1
    return 0

def add_count(tab, name):
    for t in tab:
        if t['name'] == name:
            t['count'] = t['count'] + 1
    return 0

def find_unique(name, id):
    with open(name) as target:
        js = json.load(target)
    tab = []
    for v in js:
        if not intab(tab, v[id]):
            tab.append({'name':v[id], 'count':0})
        else:
            add_count(tab, v[id])

    return tab


if len(sys.argv) == 3:
    tab = find_unique(sys.argv[1], int(sys.argv[2]))
    print json.dumps(tab, indent=4,sort_keys=True)
    print 'len = ' + str(len(tab))
