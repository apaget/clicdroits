#! /usr/bin/python
# -*- coding:utf-8 -*-

from openfisca_france import CountryTaxBenefitSystem
from openfisca_core.simulations import Simulation
from Settings import Settings
import sys
import requests
from Database import Database
import time
from SituationGenerator import SituationGenerator
from inspect import getmembers
from pprint import pprint
import os
import json
import datetime


conf = Settings().load
db = Database()
fr_sys = CountryTaxBenefitSystem()

prev_mouth = datetime.date.today() + datetime.timedelta(-2*365)

def get_json(url):
    requete = requests.get(url.encode(encoding="UTF-8"))
    if requete.status_code != 200:
        return None;
    page = requete.content
    json_data = json.loads(page, encoding=requete.encoding);
    return json_data

class Routine():
    def __init__(self, id_routine, ac):
        self.id_routine = id_routine;
        self.action = ac
        self.routine = self.load_simul_from_routine();
        self.id_action = 666
        self.result = []

    def run_on_db(self, ids, aide, simul):
        for id in ids:
            js = SituationGenerator(id[0], aide.lower()).get_json()
            simulation = Simulation(tax_benefit_system = fr_sys, simulation_json = js)
            res = simulation.calculate(aide, prev_mouth.strftime('%Y-%m'))
            self.result.append({'id_simulation' : simul['pivot']['id_simulation'], 'id_non_recourant' : id[0], 'value' : str(res[0])})

    def run_routine(self):
        print self.routine
        if self.routine:
            self.srv_start_routine()
            for simul in self.routine['simulations']:
                self.run_simulation(simul)

    def run_simulation(self, simul):
        print simul
        selected_ids = db.getAll('dos', fields=['id'], limit=['0', '20']);
        print selected_ids
        self.run_on_db(selected_ids, 'rsa', simul)#simul['algorithm']['name'].lower(), simul)


    def send_result(self):
        datasend = {'id' : self.id_routine, 'action' : self.action}
        datasend['results'] = self.result
        datasend  = json.dumps(datasend);
        print datasend
        p = requests.post(conf['baseUrl'] + '/routine/{}/action/{}/done'.format(self.id_routine, self.action), data={'data' : datasend})
        print p.content

    def load_simul_from_routine(self):
        url = conf['baseUrl'] + '/json/routine/' + str(self.id_routine);
        return get_json(url);

    def srv_start_routine(self):
        url = '/routine/' + str(self.id_routine) + '/start'


if __name__ == '__main__':
    if len(sys.argv) >= 4:
        if sys.argv[1] == 'start_routine':
            ## Faire les check ici
            print 'success'
        elif sys.argv[1] == 'run_routine':
            rt = Routine(sys.argv[2], sys.argv[3]);
            rt.run_routine()
            rt.send_result()
