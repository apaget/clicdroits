<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models\Algorithm;

use Illuminate\Database\Eloquent\Model;

class AlgorithmOrganization extends Model {
    protected $table = 'algorithms_organizations_template';
    protected $fillable = ['name'];
    public $timestamps = false; #disabled updated/created_at

    public static function findByName($name) {
        return self::where('name', $name)->first();
    }
}