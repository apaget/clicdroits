<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models\Routines;

use App\Models\Simulations\Simulation;
use App\Models\Simulations\SimulationResult;
use Illuminate\Database\Eloquent\Model;

class RoutineAction extends Model
{
    protected $table = 'routines_actions_template';
    protected $fillable = ['id_routine', 'start_date', 'end_date'];
    public $timestamps = false; #disabled updated/created_at

    public function results()
    {
        return $this->belongsToMany(SimulationResult::class, 'routines_actions_links_simulations_results_template', 'id_routine_action', 'id_simulation_result');
    }

}