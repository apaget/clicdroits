<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models\Routines;

use App\Models\Simulations\Simulation;
use Illuminate\Database\Eloquent\Model;

class RoutineActionLinkSimulationResult extends Model {
    protected $table = 'routines_actions_links_simulations_results_template';
    protected $fillable = ['id_routine_action', 'id_simulation_result'];
    public $timestamps = false; #disabled updated/created_at

}