<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models\Routines;

use App\Models\Simulations\Simulation;
use Illuminate\Database\Eloquent\Model;

class Routine extends Model {
    protected $table = 'routines_template';
    protected $fillable = ['name', 'description', 'mode', 'interval', 'status', 'init', 'last_date_start'];
    public $timestamps = false; #disabled updated/created_at

    /* RELATIONS */
    public function simulations() {
        return $this->belongsToMany(Simulation::class, 'routines_links_simulations_template', 'id_routine', 'id_simulation');
    }

    public function actions() {
        return $this->hasMany(RoutineAction::class,  'id_routine', 'id');
    }

    /* HELPERS */

    public function findByName($name) {
        return Routine::where('name', $name)->first();
    }

    /* OVERWRITTING */

    public function delete() {
        $this->actions()->each(function ($item) {
            $item->results()->detach();
        });
        $this->actions()->delete();
        $this->simulations()->detach();

        parent::delete();
    }
}
