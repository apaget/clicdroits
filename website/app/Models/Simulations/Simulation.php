<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 20:03
 */

namespace App\Models\Simulations;

use App\Models\Algorithm\Algorithm;
use App\Models\Routines\Routine;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Simulation extends Model
{
    protected $table = 'simulations_template';
    protected $fillable = ['id_algorithm', 'name', 'description', 'dataset'];
    public $timestamps = false; #disabled updated/created_at

    public function algorithm()
    {
        return $this->hasOne(Algorithm::class, 'id', 'id_algorithm');
    }

    public function results()
    {
        return $this->hasMany(SimulationResult::class, 'id_simulation', 'id');
    }

    public function routines() {
        return $this->belongsToMany(Routine::class, 'routines_links_simulations_template', 'id_simulation', 'id_routine');
    }

    public function delete()
    {
        $this->results()->delete();
        $this->routines()->detach();

        parent::delete();
    }

}