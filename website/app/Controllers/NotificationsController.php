<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 30/03/2018
 * Time: 00:19
 */

namespace App\Controllers;

use App\Models\Newsletters;
use App\Models\Notification;
use App\Models\Routines\Routine;
use App\Models\Simulation;
use App\Tools\Notifications;
use Respect\Validation\Validator as v;
use Slim\Http\Request;
use Slim\Http\Response;

class NotificationsController extends Controller {

    public function getNotifications(Request $request, Response $response, $args = null) {
        $notfications = Notification::all()->toArray();
        $notfications = array_reverse($notfications);
        array_walk($notfications, 'App\Tools\Notifications::setFormatedType');

        return json_encode($notfications);
    }

}