<?php
/**
* Created by PhpStorm.
* User: Ashk
* Date: 30/03/2018
* Time: 00:19
*/

namespace App\Controllers;

use App\Models\User;
use App\Models\Variable;
use App\Models\Formule;
use Respect\Validation\Validator as v;


class FormuleManager{

  private function getJsonData($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $data = json_decode(curl_exec($ch), true);
    return $data;
  }

  private function removeComment($str) {
        $str = preg_replace("/\*.*?\*/", "", $str);
        $str = preg_replace("/(''').*(''')/", "", $str);
        $str = preg_replace('/(""").*(""")/', "", $str);
        $str = preg_replace("/#.*?\n/", "", $str);
        return $str;
  }

  private function getChildren($formula, $variables) {
    $formula = $this->removeComment($formula . '\n');
    $children = [];
    foreach ($variables as $v) {
      $data = strstr($formula, "'". $v . "'");
      if ($data) {
        $children[] = $v;
      }
    }
    return implode(':', $children);
  }

public function updateVariable($name, $allVariables) {
    $data = $this->getJsonData("https://fr.openfisca.org/api/v21/variable/" . $name);
    if (!$data['id'])
      return;
    $variable = Variable::find($name);
      $dt = [
        'name' => $data['id'],
        'description' => $data['description'],
        'valueType' => $data['valueType'],
        'entity' => $data['entity'],
        'link' => '',
        'enum' => json_encode($data['possibleValues'])
      ];
    if($variable) {   $variable->update($dt) ;      }
    else {            Variable::create($dt);
     }
    foreach ($data['formulas'] as $key => $value) {
      $newData = [
          'name' => $data['id'],
          'formula' => $value['content'],
          'date' => $key,
          'children' => $this->getChildren($value['content'], $allVariables)
        ];
    //  $f = Formule::find($name);
      Formule::create($newData);
    }
    return $data['id'];

}
public function update_db($from, $len) {
    $data = $this->getJsonData("https://fr.openfisca.org/api/v21/variables");
    $i = 0;
    foreach ($data as $key => $value) {
        $tab[$i] = $key;
        $i++;
    }
    for ($i=$from; $i < $from + $len; $i++) {
        print_r($this->updateVariable($tab[$i], $tab));
    }
}
  public function majAllVariables($from, $len) {
    $this->update_db($from, $len);
    return $from + $len;
  }

  public function getJsonVariable($name) {
    $variable = Variable::find($name)->getAttributes();
    $js = json_decode($variable['json'], true);
    $js['link'] = $variable['link'];
    foreach ($js['children'] as &$value) {
      $tmp = Variable::find($value['name']);
      $value['link'] = $tmp->getAttributes()['link'];
    }
    $variable['json'] = json_encode($js);
    $response->write(json_encode($variable))->withStatus(200)->withHeader('Content-Type', 'application/json');
    return $response;
  }
}
