<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 30/03/2018
 * Time: 00:19
 */

namespace App\Controllers;

use App\Models\Formule;
use App\Models\User;
use App\Models\Variable;
use FormuleManager;
use Slim\Http\Request;
use Slim\Http\Response;

class VariablesController extends Controller
{

    public function majVariables(Request $request, Response $response)
    {
        $manager = new \App\Controllers\FormuleManager();
        $from = $request->getAttribute('from');
        $len = $request->getAttribute('to');
        $data = $manager->majAllVariables($from, $len);
        $response->write(json_encode($data, JSON_PRETTY_PRINT))->withStatus(200)->withHeader('Content-Type', 'application/json');
    }

    public function getVariables(Request $request, Response $response)
    {
        $variables = Variable::all();
        $this->render($response, 'pages/variables/variables.twig', ['variables' => $variables]);
    }

    public function renderInfo(Request $request, Response $response, $args = null)
    {
        $name = $request->getAttribute('name');
        $v = $this->getInfoVar($name);
        $this->render($response, 'pages/variables/variable_info.twig', ['variable' => $v]);
    }

    public function getInfoVar($name)
    {
        $variable = Variable::find($name);
        if ($variable) {
            $v = $variable->getAttributes();
            $tab = [];
            $fomulas = Formule::all()->where('name', '=', $name);
            foreach ($fomulas as $formula) {
                $tab[] = $formula->getAttributes();
            }
            $v['formulas'] = $tab;
            $v['enum'] = json_decode($v['enum'], true);
            return $v;
        }
        return null;
    }

    private function getJsonEnfant($name)
    {
        if ($name == "")
            return null;
        $formule = Formule::select("children")->where('name', '=', $name)->orderBy('date', 'DESC')->first();
        $variable = Variable::find($name);

        if($variable){
          $variable = $variable->getAttributes();
          $json = [];
          $json['name'] = $name;
          $json['children'] = null;
          $json['link'] = $variable['link'];

        if($formule){
          $c = $formule->getAttributes()['children'];
          $tabchild = explode(':', $c);
          $json['nb_children'] = $c === '' ? 0 : count($tabchild);
        //  dd($json);
        }
          return $json;
        }
        return ['name' => $name, 'parent' => null, 'nb_children' => 0, 'children' => null];
    }

    public function getJsonVariable(Request $request, Response $response, $args = null)
    {
        $name = $request->getAttribute('name');
        $formule = Formule::select("children")->where('name', '=', $name)->orderBy('date', 'DESC')->limit(1)->get();
        $variable = Variable::find($name)->getAttributes();
        $json = [];
        if ($formule[0] && $name != "") {
            $json['name'] = $name;
            $json['parent'] = 'ROOT';
            $json['children'] = [];
            $v = $formule[0]->getAttributes()['children'];
            $tab = explode(':', $v);
            $json['nb_children'] = $v === '' ? 0 : count($tab);
            $this->db->getConnection()->beginTransaction();
            foreach ($tab as $value) {
                $tt = $this->getJsonEnfant($value);
                if ($tt)
                    $json['children'][] = $tt;
            }
            $this->db->getConnection()->commit();
            $variable['json'] = json_encode($json);
        } else {
            $variable = null;
        }
        $response->write(json_encode($variable))->withStatus(200)->withHeader('Content-Type', 'application/json');
        return $response;
    }

    public function getVariable(Request $request, Response $response, $args = null)
    {
        $name = $request->getAttribute('name');
        $v = $this->getInfoVar($name);
        $this->render($response, 'pages/variables/variable.twig', ['variable' => $v]);
    }

    public function getHome(Request $request, Response $response) {
        $v = $this->getInfoVar('clicdroits');
        $this->render($response, 'pages/home.twig', ['variable' => $v]);
    }

    public function postVariablesSetLink(Request $request, Response $response)
    {
        $name = $request->getParam('name');
        $linkValue = $request->getParam('link');
        $variable = Variable::where('name', $name)->update([
            'link' => $linkValue
        ]);
        return (json_encode('success'));
    }

}
