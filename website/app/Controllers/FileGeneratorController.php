<?php

namespace App\Controllers;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Variable;
use \ZipArchive;
use App\Models\Algorithm\Algorithm;

class FileGeneratorController extends Controller {

	public function genFiles($name_of_admin, $aide, $deep) {
		$var = Variable::find($aide);
		echo $aide;
		if (!$deep || !$var)
			return 0;
		$v = $var->get_last_link();
		if($v['link'] != "") {
			$v['link'] = json_decode($v['link'], true);
			$content = $this->container->get('view')->getEnvironment()->render('template_loaders.twig', ['variable' => $v]);
			file_put_contents('../client/' . $name_of_admin . '/' . $v['name'] . '_loader.py', $content);
			return 1;
		}
		else {
			$children = explode(':', $v['child']['children']);
			foreach ($children as $child) {
				if ($this->genFiles($name_of_admin, $child, $deep - 1) == 0) {
					return 0;
				}
			}
			return 1;
		}
	}

    public function genZip(Request $request, Response $response) {
		$name_of_admin = time() . '_caf';
		mkdir('../client/' . $name_of_admin, 0777, true);
		$mod = new Algorithm();
		$aides = $mod->all();
		//dd();
		foreach($aides as $aide) {
		//	var_dump($aide);
			print $aide->getAttributes()['name'] . '</br>';
			if ($this->genFiles($name_of_admin, $aide->getAttributes()['name'], 100) == 1) {
				//shell_exec('rm -rf ../client/'. $name_of_admin);
				//return $response->write('error')->withStatus(200);
				//echo $aide;
			}
		}
		file_put_contents('../client/' . $name_of_admin . '/__init__.py', $content);
		return $response->write('success')->withStatus(200);
    }
}
