<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 30/03/2018
 * Time: 00:19
 */

namespace App\Controllers;

use App\Models\User;
use App\Models\Variable;
use App\Models\Link;
use Respect\Validation\Validator as v;
use Slim\Http\Request;
use Slim\Http\Response;
use FormuleManager;

class LinkController extends Controller {
    public function SetLink(Request $request, Response $response) {
        $info = $request->getParam('info');
        if(!$info || !$info['aide'] || !$info['variable'])
          return ;
        $aide = $info['aide'];
        $link = Link::where(['aide' =>  $aide, 'variable' => $info['variable']]);
        $item = $link->get();
        $d = [
          'aide' => $aide,
          'variable' => $info['variable'],
          $info['name_var'] => $info['var']
        ];
        if(count($item)){
          $link->update($d);
        }
        else {
          Link::create($d);
        }

    }

}
