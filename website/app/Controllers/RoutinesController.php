<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 30/03/2018
 * Time: 00:19
 */

namespace App\Controllers;

use App\Models\Routines\Routine;
use App\Models\Routines\RoutineAction;
use App\Models\Routines\RoutineActionLinkSimulationResult;
use App\Models\Routines\RoutineLinkSimulation;
use App\Models\Simulations\Simulation;
use App\Models\Simulations\SimulationResult;
use Slim\Http\Request;
use Slim\Http\Response;

class RoutinesController extends Controller {

    /* Routines */
    public function getRoutines(Request $request, Response $response) {
        $routines = Routine::all();

        $this->render($response, 'pages/routines/routines.twig', ['routines' => $routines]);
    }

    /* Add Routines */
    public function getAddRoutine(Request $request, Response $response) {
        $simulations = Simulation::all();
        $this->render($response, 'pages/routines/add.twig', ['simulations' => $simulations]);
    }

    public function postAddRoutine(Request $request, Response $response) {
        $name = $request->getParam('name');
        $description = $request->getParam('description');
        $simulations = $request->getParam('simulations');
        $mode = $request->getParam('mode');
        $interval = $request->getParam('interval');

        $routine = Routine::create([
            'name' => $name,
            'description' => $description,
            'mode' => $mode,
            'interval' => $interval,
            'init' => 0,
            'status' => 0,
            'last_date_start' => null
        ]);

        RoutineLinkSimulation::where('id_routine', $routine->id)->delete();
        foreach ($simulations as $k => $v)
            RoutineLinkSimulation::create([
                'id_routine' => $routine->id,
                'id_simulation' => $v
            ]);
        $this->notifications->addNotification("create", "Création d'une nouvelle routine", "Routine : $routine->name");
        $this->flash->addMessage('success', 'La Routine ' . $routine->name . ' a bien été créée!');
        return $response->withRedirect($this->router->pathFor('routines'));
    }

    /* Routine N */
    public function getRoutine(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $routine = Routine::find($id);
        $simulations = Simulation::all();

        $this->render($response, 'pages/routines/routine.twig', ['routine' => $routine, 'simulations' => $simulations]);
    }

    public function getJsonRoutine(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');

        $routine = \App\Models\Routines\Routine::with('simulations.algorithm')->find($id)->toArray();
        return json_encode($routine);
    }

    public function getRoutineResults(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');

        $routine = Routine::with('actions.results')->find($id);
        foreach ($routine['actions'] as $action => &$v) {
            $simulations = [];
            foreach ($v['results'] as $key => $value) {
                $id = $value['id_simulation'];
                if (!array_key_exists($id, $simulations)) {
                    $simulations[$id] = $value['simulation'];
                    $simulations[$id]['results'] = new \ArrayObject();
                }
                $rest = $value;
                unset($rest['simulation']);
                $simulations[$id]['results'][] = $rest;
            }
            unset($v['results']);
            $v['simulations'] = $simulations;
        }

        $this->render($response, 'pages/routines/results.twig', ['routine' => $routine]);
    }

    public function postRoutine(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $name = $request->getParam('name');
        $description = $request->getParam('description');
        $simulations = $request->getParam('simulations');
        $mode = $request->getParam('mode');
        $interval = $request->getParam('interval');

        $routine = Routine::where('id', $id)->first();
        $routine->update([
            'name' => $name,
            'description' => $description,
            'mode' => $mode,
            'interval' => $interval,
        ]);

        RoutineLinkSimulation::where('id_routine', $id)->delete();
        foreach ($simulations as $k => $v)
            RoutineLinkSimulation::create([
                'id_routine' => $id,
                'id_simulation' => $v
            ]);
        $this->notifications->addNotification("update", "Modification d'une routine", "Routine : $routine->name");
        $this->flash->addMessage('success', 'La routine ' . $routine->name . ' a été mis à jour!');
        return $response->withRedirect($this->router->pathFor('routines'));
    }

    public function getDeleteRoutine(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $routine = Routine::with('actions', 'simulations')->where('id', $id)->first();

        $this->notifications->addNotification("delete", "Suppression d'une routine", "Routine : $routine->name");
        $this->flash->addMessage('success', 'La Routine ' . $routine->name . ' a bien été supprimée!');

        $routine->delete();
        return $response->withRedirect($this->router->pathFor('routines'));
    }

    /* Start Routine */

    public function getRoutineStart(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $routine = Routine::find($id);

        $files = $request->getUploadedFiles();
        if ($routine->status === 1) {
            $this->flash->addMessage('error', 'La routine ' . $routine->name . ' est déjà en marche!');
            return $response->withRedirect($this->router->pathFor('routines'));
        }

        if ($routine->mode === 0 && $routine->init === 1) {
            $this->flash->addMessage('error', 'La routine ' . $routine->name . ' est en mode automatique!');
            return $response->withRedirect($this->router->pathFor('routines'));
        }

        $now = date('Y-m-d H:i:s');
        $routineAction = RoutineAction::create([
            'id_routine' => $id,
            'start_date' => $now
        ]);

        $cmd_start =  $this->settings['simulator']['internal_cmd'] . " " . $this->settings['simulator']['script_name'] . " start_routine " . $id . " " .  $routineAction->id . ' 2>&1';
        $cmd_run =  $this->settings['simulator']['internal_cmd'] . " " . $this->settings['simulator']['script_name'] . " run_routine " . $id . " " .  $routineAction->id;
        if (count($files) > 0) {
          $filename = 'assets/saved_csv/' . $files['file']->getClientFilename();
          $files['file']->moveTo($filename);
          $cmd_run =  $this->settings['simulator']['internal_cmd'] . " " . $this->settings['simulator']['script_name_2'] . " 10 " . $filename . " " . $id . " " .  $routineAction->id . " af";
        }

        $output = shell_exec($cmd_start);
        switch (substr($output, 0, 7)) {
            case 'success':
				exec('nohup ' . $cmd_run . ' >> /dev/null 2>&1 & echo $!', $pid);
                $routine->update([
                    'status' => 1,
                    'last_date_start' => $now,
                    'init' => 1
                ]);
                break;
            default:
                RoutineAction::find($routineAction->id)->delete();
                $this->flash->addMessage('error', 'La routine ' . $routine->name . ' rencontre des difficultés pour se lancer');
                return $response->withRedirect($this->router->pathFor('routines'));
        }
        $this->notifications->addNotification("start", "Lancement d'une routine", "Routine : $routine->name");
        $this->flash->addMessage('success', 'La routine ' . $routine->name . ' vient de se lancer!');
        return $response->withRedirect($this->router->pathFor('routine/results', array('id' => $id)));
    }

    public function postRoutineDone(Request $request, Response $response, $args = null) {
        $id = $request->getAttribute('id');
        $actionId = $request->getAttribute('action');
        $results = json_decode($request->getParam('data'), true); // format ['id_simulation', 'id_non_recourant', 'value']

		$results = $results['results'];
        $routine = Routine::find($id);
        $routineAction = RoutineAction::find($actionId);

        if (!$routine)
            return json_encode('failLoadRoutine');
        if (!$routineAction)
            return json_encode('failLoadRoutineAction');

        $now = date('Y-m-d H:i:s');

        foreach ($results as $value) {
            $simulationResult = SimulationResult::create([
                'id_simulation' => $value['id_simulation'],
                'id_non_recourant' => $value['id_non_recourant'],
                'value' => $value['value'],
                'date' => $now,
                'status' => intval($value['status']),
                'missing' => $value['missing']
            ]);

            $simulationResult->refresh();
            RoutineActionLinkSimulationResult::create([
                'id_routine_action' => $routineAction->id,
                'id_simulation_result' => $simulationResult->id
    			]);

        }

        $routineAction->update([
            'end_date' => $now
        ]);

        $routine->update([
            'status' => 0
        ]);

        $this->notifications->addNotification("done", "Une routine vient de se terminer!", "Routine : $routine->name");
        return json_encode('success');
    }
}
