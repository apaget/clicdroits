<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 23/04/2018
 * Time: 16:03
 */

namespace App\Tools;

use App\Models\Notification;

class Notifications extends Tool{
    public function addNotification($type, $title, $content) {
        Notification::create([
            'type' => $type,
            'title' => $title,
            'content' => $content,
            'publish' => date("Y-m-d H:i:s")
        ]);
    }

    public static function setFormatedType(&$item, $key) {
        $format = [
            'delete' => [
                'icon' => 'fa-times',
                'bg_class' => 'bg-danger'
            ],
            'update' => [
                'icon' => 'fa-paint-brush',
                'bg_class' => 'bg-primary'
            ],
            'create' => [
                'icon' => 'fa-coffee',
                'bg_class' => 'bg-info'
            ],
            'done' => [
                'icon' => 'fa fa-check',
                'bg_class' => 'bg-success'
            ],
            'start' => [
                'icon' => 'fa-bullhorn',
                'bg_class' => 'bg-info'
            ]
        ];
        $item['style'] = $format[$item['type']];
    }
}