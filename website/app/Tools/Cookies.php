<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 25/04/2018
 * Time: 23:10
 */

namespace App\Tools;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Cookies extends Tool
{

    public function delete(Response $response, $key)
    {
        $cookie = urlencode($key) . '=' .
            urlencode('deleted') . '; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/; secure; httponly';
        $response = $response->withAddedHeader('Set-Cookie', $cookie);
        return $response;
    }

    public function add(Response $response, $cookieName, $cookieValue)
    {
        $expirationMinutes = 10;
        $expiry = new \DateTimeImmutable('now + ' . $expirationMinutes . 'minutes');
        $cookie = urlencode($cookieName) . '=' .
            urlencode($cookieValue) . '; expires=' . $expiry->format(\DateTime::COOKIE) . '; Max-Age=' .
            $expirationMinutes * 60 . '; path=/; secure; httponly';
        $response = $response->withAddedHeader('Set-Cookie', $cookie);
        return $response;
    }

    public function get(Request $request, $cookieName)
    {
        $cookies = $request->getCookieParams();
        return isset($cookies[$cookieName]) ? $cookies[$cookieName] : null;
    }

}

?>