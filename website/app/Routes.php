<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 31/03/2018
 * Time: 19:34
 */

use App\Controllers as Controllers;

//phpinfo();
$app->post('/link', Controllers\VariablesController::class . ':postVariablesSetLink')->setName('variables/link');
$app->get('/', Controllers\VariablesController::class . ':getHome')->setName('home');
/* Variables */
$app->get('/variables', Controllers\VariablesController::class . ':getVariables')->setName('variables');
$app->get('/variables/update/{from}/{to}', Controllers\VariablesController::class . ':majVariables')->setName('variables/update');

$app->get('/variable/{name}', Controllers\VariablesController::class . ':getVariable')->setName('variable');
$app->get('/variable/info/{name}', Controllers\VariablesController::class . ':renderInfo')->setName('getInfo');

/* Simulation */
$app->get('/simulations', Controllers\SimulationsController::class . ':getSimulations')->setName('simulations');

$app->get('/simulation/add', Controllers\SimulationsController::class . ':getAddSimulation')->setName('simulation/add');
$app->post('/simulation/add', Controllers\SimulationsController::class . ':postAddSimulation');

$app->get('/simulation/{id}', Controllers\SimulationsController::class . ':getSimulation')->setName('simulation');
$app->post('/simulation/{id}', Controllers\SimulationsController::class . ':postSimulation');
$app->get('/generateLoader', Controllers\FileGeneratorController::class . ':genZip')->setName('FileGenerator');

$app->get('/simulation/{id}/delete', Controllers\SimulationsController::class . ':getDeleteSimulation')->setName('simulation/delete');

/* Routines */
$app->get('/routines', Controllers\RoutinesController::class . ':getRoutines')->setName('routines');
$app->get('/routine/{id}/results', Controllers\RoutinesController::class . ':getRoutineResults')->setName('routine/results');
$app->any('/routine/{id}/start', Controllers\RoutinesController::class . ':getRoutineStart')->setName('routine/start');
$app->post('/routine/{id}/action/{action}/done', Controllers\RoutinesController::class . ':postRoutineDone')->setName('routine/action/done');
$app->get('/routine/add', Controllers\RoutinesController::class . ':getAddRoutine')->setName('routine/add');
$app->post('/routine/add', Controllers\RoutinesController::class . ':postAddRoutine');

$app->get('/routine/{id}', Controllers\RoutinesController::class . ':getRoutine')->setName('routine');
$app->post('/routine/{id}', Controllers\RoutinesController::class . ':postRoutine');

$app->get('/routine/{id}/delete', Controllers\RoutinesController::class . ':getDeleteRoutine')->setName('routine/delete');

/* Notifications */
$app->get('/notications[/{notseen}]', Controllers\NotificationsController::class . ':getNotifications')->setName('notifications');

$app->get('/json/routine/{id}', Controllers\RoutinesController::class . ':getJsonRoutine')->setName('JSONroutine');
$app->get('/json/variable/{name}', Controllers\VariablesController::class . ':getJsonVariable')->setName('JSONvariable');
