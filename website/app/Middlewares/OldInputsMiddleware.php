<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 01/04/2018
 * Time: 16:36
 */

namespace App\Middlewares;

use Slim\Http\Request;
use Slim\Http\Response;

class OldInputsMiddleware extends Middleware
{
    public function __invoke(Request $request, Response $response, $next)
    {
        if (isset($_SESSION['old']))
            $this->container->view->getEnvironment()->addGlobal('old', $_SESSION['old']);
        $_SESSION['old'] = $request->getParams();
        $response = $next($request, $response);

        return $response;
    }
}