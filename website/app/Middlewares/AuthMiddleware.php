<?php
/**
 * Created by PhpStorm.
 * User: Ashk
 * Date: 01/04/2018
 * Time: 16:36
 */

namespace App\Middlewares;

use Slim\Http\Request;
use Slim\Http\Response;

class AuthMiddleware extends Middleware
{
    public function __invoke(Request $request, Response $response, $next)
    {
        if(!$this->container->auth->check()) {
            $this->container->flash->addMessage('error', 'Vous devez être connecté pour accéder à cette page.');
            return $response->withRedirect($this->container->router->pathFor('auth'));
        }
        $response = $next($request, $response);
        return $response;
    }
}