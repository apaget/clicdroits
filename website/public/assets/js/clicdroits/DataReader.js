var period_detect = ["period.this_month", "period.last_month", "period.this_year", "period.last_year", "period.n_2", "period.last_3_months"]

function replaceAll(str, find, replace) {
	return str.replace(new RegExp(find), replace);
}

function get_link_template(name) {
	return "<div><a href='/variable/"+ name +"'>" + name + "</a></div>"
}

function remove_all_link() {
	$('[for]').each(function(idx, elem){
		$(elem).parent().parent().remove();
	})
}

var update_link_input = function(link) {
	remove_all_link();
	if (link == undefined || link.trim() == '') {
		add_input("NewName", "");
	}
	else {
		data = JSON.parse(link);
		for (var k in data) {
			add_input(k, data[k]);
		}
	}
}

var update_screen_info = function(resp) {

	$('#var_info_template').remove();
	$('#var_info_template_container').append(resp);
	//update_link_input(json_data['link']);
	$('code').each(function(idx, elem) {
		Prism.highlightElement(elem);
	})
	/*
	$('#selected_children').html("");
	var child = json_data['children'].split(':');
	for (var i = 0, len = child.length; i < len; i++) {
		$('#selected_children').append(get_link_template(child[i]));
	}*/
}

var update_json_info = function(name) {
	$.ajax({
		url: "info/" + name,
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(thrownError);
		},
		success: function(resp) {
			update_screen_info(resp);
			init_slider();
			init_check_box();
			$('#lin').removeClass('active');
			$('#inf').addClass('active');
		}
	})
}


var add_input = function(name, link) {
	var cp = $('<input class="form-control" type="text"></input>');
	cp.attr('for', 'selected_link_' + $('[for]').length );
	cp.attr('id', '');
	cp.attr('value', name);
	var elem = $("<tr></tr>");

	elem.append("<td>" + cp[0].outerHTML + "</td>")

	var cp = $('<input class="form-control" type="text"></input>');
	cp.attr('id', 'selected_link_' + $('[for]').length );
	cp.attr('value', link);
	elem.append("<td>" + cp[0].outerHTML + "</td>")
	$('tbody').children().eq(0).after(elem);
}
