var selected = undefined;
var current_node;
var interval;
focus.x = 50
focus.y = 50
var getFiles = function(data) {
	var aide = d3.selectAll('g.node').filter(function(d) {return d.parent == 'ROOT'}).text();
	$.post("/generateLoader", {"aide" : aide}).done(function(resp) {
		if (resp == 'success')
			alert_flash('success', "Vos loaders ont ete generer dans client");
		else if (resp == 'error')
			alert_flash('error', "Vous sembler ne pas avoir asser de donner pour simuler l'aide");
		else
			alert(resp);
	});
}

var update_link = function() {
	var data = {};
	$('[linkfor]').each(function(idx, elem){
		var id = $(elem).attr('linkfor');
		Object.assign(data, {[$(elem).val()] : $('#' + id).val()});
	})
	$.post('/link', {'name' : current_node.name, 'link' : JSON.stringify(data)}, function(){
		alert_flash('success', 'Link set');
	})
	console.log(data);
}

var get_simulation = function(data) {
    $.post("/simulator", {aide : 'rsa', json : JSON.stringify(data)}).done(function(resp){
        alert_notifications('success', "Votre aide est de " + resp);
    })
}

var click = function(d) {
    current_node = d;
		if(selected.rot){
		focus.x = -current_node.x + 600
		focus.y = -current_node.y + 300
		}
		else {
			focus.x = 0
			focus.y = 0
		}
    if (d.children) {
        d._children = d.children;
        d.children = null;
    } else {
        d.children = d._children;
        d._children = null;
    }
    if (d.children == null && d._children == null) {
        if (exist_in_json(d.name, selected.root) == 0) {
            add_in_json(d.name, selected.root, d.parent.name, d, selected);
        }
        else {
        }
    }
		setTimeout(function(){node_calc(d, selected);}, 100);

    update(d, selected)
    update_json_info(d.name);
}


var update = function(source, viewer) {
    var nodes = viewer.tree.nodes(viewer.root).reverse();
    var links = viewer.tree.links(nodes);
    nodes.forEach(function(d) {if (viewer.rot) {d.x *= 1.5;d.y = d.depth * 220; } else {d.y = d.depth * 350; } });
		if (!viewer.rot) {
			focus.x = 0
			focus.y = 0
		}



    // --------------  Node ----------------- //
    var node = viewer.svg.selectAll("g.node").data(nodes, function(d) { return d.id || (d.id = ++viewer.it); });

    // --------------  Node enter ----------------- //
    var nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) {
					if(viewer.rot){
	          return "translate(" +( source.x0+focus.x)  + "," + (source.y0+focus.y) + ")";
					}
          return "translate(" +( source.y0+focus.y)  + "," + (source.x0+focus.x) + ")"; })
        .on("click", click)

    nodeEnter.append("circle")
				.attr('r', 0)
        .style("fill", "lightsteelblue");

		function set_infos(d){

		if (!viewer.infos[d.name]) {
			viewer.infos[d.name] = {"name" : d.name};
			viewer.infos[d.name]["nb_children"] = d.nb_children;
			viewer.infos[d.name]["calculate"] = 0;
			viewer.infos[d.name]["fill"] = 0;
			if (d.link && d.link.length > 0) {
				viewer.infos[d.name]["fill"] = 1;
			}
			viewer.infos[d.name]["entity"] = 'famille';//d.entity;
		}
		return d.name;
		}

    nodeEnter.append("text")
        .attr("x", function(d){
					if(d.name =='Clicdroits'){
						return -20;
					}
					return 15;
				})
        .attr("dy", ".35em")
        .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
        .text(function(d) { return set_infos(d);})
        .style("fill-opacity", 1e-6);
				if (viewer.rot) {
					nodeEnter.select("text").attr('transform', 'rotate(-10)');
				}
    // --------------  Node update ----------------- //

    var nodeUpdate = node.transition()
        .duration(viewer.duration)
        .attr("transform", function(d) {
						if (viewer.rot){
							return "translate(" + (d.x + focus.x)+ "," + (d.y + focus.y) + ")";
						}
					return "translate(" + (d.y + focus.y)+ "," + (d.x+focus.x) + ")"; });

    nodeUpdate.select("circle")
        .attr("r", function(d) {
					if (d.name == 'Clicdroits'){
						return 20;
					}
						return 10;
				})
        .style('fill', function(d){
            var color = "#000";
            if (viewer.infos[d.name]['fill']) {
                return "LightGreen";
            }
            else if (viewer.infos[d.name]["calculate"]) {
				viewer.infos[d.name]["calculate"] = 1;
                return "LightGreen";
            }
            else {
                color = "lightsteelblue";
            }
            if (viewer.infos[d.name].nb_children == 0) {
                color = "Orange";
            }
            return color;
        }).
    style('stroke-width', '0px');


    nodeUpdate.select("text")
        .style("fill-opacity", 1);

    // --------------  Node exit ----------------- //

    var nodeExit = node.exit().transition()
        .duration(viewer.duration)
        .attr("transform", function(d) {
						if (viewer.rot){
							return "translate(" + (source.x + focus.x)+ "," + (source.y + focus.y) + ")";
						}
						return "translate(" + (source.y+focus.y) + "," + (source.x+focus.x) + ")"; })
        .remove();

    nodeExit.select("circle")
        .attr("r", 1e-6);

    nodeExit.select("text")
        .style("fill-opacity", 1e-6);

    // --------------  Link  ----------------- //

    var link = viewer.svg.selectAll("path.link")
        .data(links, function(d) { return d.target.id; });


    // --------------  Link  enter ----------------- //

    var diagonal = d3.svg.diagonal().projection(function(d) {	if(viewer.rot == 1) { return [d.x + focus.x , d.y + focus.y];} return [d.y, d.x]; });
    link.enter().insert("path", "g")
        .attr("class", "link")
        .attr("d", function(d) {
            var o = {x: source.x0, y: source.y0};
            return diagonal({target: o, source: o});
        });

    // --------------  Link  update ----------------- //

    link.transition()
        .duration(viewer.duration)
        .attr("d", diagonal);

    // --------------  Link  exit ----------------- //

    link.exit().transition()
        .duration(viewer.duration)
        .attr("d", function(d) {
					var o = {x: source.x0, y: source.y0};
            return diagonal({source: o, target: o});
        })
        .remove();

    nodes.forEach(function(d) {
        d.x0 = d.x;
        d.y0 = d.y;

    });
    d3.select(self.frameElement).style("height", "800px");
}

function SvgViewer (dom_elem, variable, height, rot){
		this.rot = rot
    this.margin = {top: 20, right: 120, bottom: 20, left: 120};
    this.width = 6000 - this.margin.right - this.margin.left;
    this.height = height;
    this.variable = variable;
    console.log(get_db_info_json(variable));
    this.json = JSON.parse(get_db_info_json(variable)).json;
    this.json = JSON.parse(this.json);
    this.tree = d3.layout.tree().size([this.height, this.width]);
    this.diagonal = d3.svg.diagonal().projection(function(d) { return [d.y, d.x]; });
    this.svg = d3.select(dom_elem).append("svg")
        .attr("width", this.width + this.margin.right + this.margin.left)
        .attr("height", this.height + this.margin.top + this.margin.bottom).append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
    this.it = 0;
    this.duration = 700;
    this.root = this.json;
    this.root.x0 = height / 2;
    this.root.y0 = 0;
	this.infos = {}
	d3.selectAll
    this.nodes = this.tree.nodes(this.root).reverse();
    collapse(this.root);

    this.refresh_graph = function() {
        this.root = this.json;
        this.root.x0 = height / 2;
        this.root.y0 = 0;
        update(this.root, this);
    }
    this.refresh_graph();
}

var node_calc = function(d, v) {
	if (node_can_be_calculated(d, v)) {
		v.infos[d.name]['calculate'] = 1;
		setTimeout(function(){
		update(d, v);
		node_calc(d.parent, v);}, 1000);
	}
	else if (d == "ROOT") {}
	else if (selected.infos[d.name]['fill']) {
		setTimeout(function(){
		update(d, v);
		node_calc(d.parent, v);}, 1000);
	}
}

var update_info = function(aide, va, varname, v){
	var d = {
				'variable' : va,
				'aide' : aide,
				'name_var' : varname,
				'var' : v
			};
			$.ajax({
				url: '/link',
				method: 'POST',
				data: {'info' : d},
				success: function(data){
					alert_flash('success', 'Variable mis a jour');
				},
				error: function(a, b, c){
			console.log('whta');
					$('body').html(a.responseText);
				}
			});
}

var update_default = function(){
	if($('#defaultValueTable').length){
		var c = $('#defaultValueTable').find('input:checked');
		if (c.length == 1){
			update_info($('#v_name').html(), current_node.name, 'default', c.parent().parent().children().eq(2).html());
		}
		else if (c.length > 1){
			alert_flash('error', 'Vous dever avoir qu\'une seul variable par defaut');
		}
		else {
			alert_flash('error', 'Vous n\'avez rien selectioner');
		}

	} else {
		update_info($('#v_name').html(), current_node.name, 'default', $('#defaultValue').val());
	}
}

var init_slider = function(){
	$( "#slider").slider({
		min: 1,
		max: 10,
		step: 1,
		slide:function(e, ui){
			console.log(e, ui);
			var span = $( "#slider").find('span');
			span.html(ui.value);
			span.addClass('d-flex');
			span.addClass('justify-content-around');
			span.css('transform', 'scale('+ (1 + (ui.value / 10)) +')')
			span.css('align-items', 'center');

		},
		stop:function(e, ui){
			console.log();
			update_info($('#v_name').html(), current_node.name, 'importance', ui.value);
		}
	});
}

var init_check_box = function() {
	$('#v_state').find('input').each(function(idx, elem) {
			$(elem).click(function(){
				$('#v_state').find('input').each(function(idx, elem) {
					$(elem).prop('checked', false)
					$('[pageFor=' + $(elem).attr('id') + ']').css('display', 'none');
				});
				$(elem).prop('checked', true);
				console.log(elem);
				update_info($('#v_name').html(), current_node.name, 'status', $(elem).attr('id').split('-')[1]);
				$('[pageFor=' + $(elem).attr('id') + ']').css('display', 'block');
			});
	});
}

var valid_callback = function(v) {
    v = v.data;
    var nodes = v.tree.nodes(v.root).reverse();
    nodes.forEach(function(d) { d.y = d.depth * 250; }); // default 180
		console.log('fdp', current_node.name);
	d3.selectAll('g.node').filter(function(c) { return current_node.name == c.name }).each(function(d){
		v.infos[d.name]['fill'] = 1;
		setTimeout(function(){
		if (d.children) {
			d._children = d.children;
			d.children = null;
		}
		update(d, v);
		node_calc(d.parent, v);}, 600);
	})
    update(v.root, v);
};

var room = 1;
function education_fields() {

    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass"+room);
	var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="row"> \
		<div class="col-md-3 col-md-offset-5">\
			<div class="form-group"> \
				<input type="text" class="form-control" linkfor="req_"'+$('[linkfor]').length+' id="Schoolname" name="Schoolname[]" value="" placeholder="Name"> \
			</div> \
		</div> \
		<div class="col-sm-9 "> \
			<div class="form-group"> \
				<div class="input-group"> \
				<input type="text" class="form-control" id="req_"'+$('[linkfor]').length+' name="Major[]" value="" placeholder="Database request"> \
					<div class="input-group-btn"> \
						<button class="btn btn-danger" type="button"  onclick="remove_education_fields('+room+');"> <span class="fa fa-minus" aria-hidden="true"></span> </button> \
					</div> \
				</div> \
			</div> \
		</div> \
		</div>';

    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
	   $('.removeclass'+rid).remove();
   }
