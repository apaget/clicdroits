FROM nimmis/apache-php7
RUN apt-get update && apt-get install -y apt-file && apt-get install -y vim 
RUN apt-get install -y git
RUN apt-get install -y zsh
RUN apt-get install -y php-pear libyaml-dev
RUN apt-get install -y php7.0-dev
RUN apt-get install python2.7-dev libmysqlclient-dev -y
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python2.7 get-pip.py && pip install openfisca_france requests MySQL-python
RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
RUN apt-get install -y mysql-server
RUN pecl install yaml
EXPOSE 80
COPY website /var/www/html/website
COPY php/php.ini /etc/php/7.0/apache2/
COPY apache/.htaccess /var/www/html/website/public
COPY apache/clicdroits_host.conf /tmp
COPY run.sh /tmp
WORKDIR /var/www/html/website
ENTRYPOINT ["/bin/bash", "/tmp/run.sh"]

