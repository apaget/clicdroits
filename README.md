# Clic'Droits Dockerfile

Clic'droits is a team of 4 students from 42 and Science Po Paris, who's worked
with CAF of Paris to find people who dont have all their rigths.

To do this we have use [OpenFisca](https://fr.openfisca.org/) to calculate people rigths and compare it to their actual rights.

This dockerfile contain all files needed to run the project.

<img src="screenshoot/1.png" width="250" height="175"></img>
<img src="screenshoot/2.png" width="250" height="175"></img>
<img src="screenshoot/3.png" width="250" height="175"></img>

## Getting Started

Build
-----

```
git clone https://gitlab.com/apaget/clicdroits
cd clicdroits
docker build -t clicdroits . 
```
Usage
-----

Run the following command :
```
docker run -it -p 8100:80 -p 8889:3306 clicdroits /bin/zsh  
```
and click on the link in the terminal to access the interface to explore.
