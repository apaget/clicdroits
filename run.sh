#!/usr/bin/bash


#Setting up perso 
chsh -s /bin/zsh

#Setting up Myswl 
/usr/bin/find /var/lib/mysql -type f -exec /usr/bin/touch {} \;
export HOME=/root
mkdir /var/lib/mysqld
mkdir /var/www/html/website/public/assets/saved_csv
chgrp www-data /var/www/html/website/public/assets/saved_csv
chmod g+rws /var/www/html/website/public/assets/saved_csv
usermod -d /var/lib/mysql/ mysql 2> /dev/null
service mysql start 2> /dev/null
mysql -u root -e 'create database clicdroits'
mysql -u root -e 'create database clicgauche'
mysql -u root clicdroits < sql/clicdroits.sql

#Setting up apache
a2enmod rewrite > 666
cat /tmp/clicdroits_host.conf > /etc/apache2/sites-enabled/000-default.conf
service apache2 start 2>/dev/null

echo 'Env pret : http://localhost:8100/'
echo $2
exec "$@";
